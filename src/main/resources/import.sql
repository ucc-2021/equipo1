INSERT INTO patient (id, age, gender, height, name, weight) VALUES (1, 78, "Male", 1.80, "Joseph", 100);
INSERT INTO patient (id, age, gender, height, name, weight) VALUES (2, 83, "Female", 1.60, "Alexa", 81);
INSERT INTO patient (id, age, gender, height, name, weight) VALUES (3, 45, "Female", 1.55, "Katy", 67);
INSERT INTO patient (id, age, gender, height, name, weight) VALUES (4, 32, "Male", 1.73, "Arthur", 70);


INSERT INTO disease (id, description, name) VALUES (1, "Es la enfermedad del sistema circulatorio", "Hipertensión");
INSERT INTO disease (id, description, name) VALUES (2, "Es una de las enfermedades", "Aterosclerosis");
INSERT INTO disease (id, description, name) VALUES (3, "Generalmente, este problema en el sistema", "Infarto al miocardio");
INSERT INTO disease (id, description, name) VALUES (4, "Las fallas en el sistema", "Arritmias");


INSERT INTO patient_disease (id_patient, id_disease) VALUES (1, 4);
INSERT INTO patient_disease (id_patient, id_disease) VALUES (2, 3);
INSERT INTO patient_disease (id_patient, id_disease) VALUES (3, 2);
INSERT INTO patient_disease (id_patient, id_disease) VALUES (4, 2);

