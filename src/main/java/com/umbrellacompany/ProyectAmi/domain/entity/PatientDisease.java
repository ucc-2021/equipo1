package com.umbrellacompany.ProyectAmi.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "patient_disease", schema = "ami",
uniqueConstraints = {
        @UniqueConstraint(name = "patient_disease_UN", columnNames = {"id_patient", "id_disease"})
})

public class PatientDisease {

    @Id

    @Column(name = "id_patient", nullable = false)
    private Long idPat;

    @JoinColumn(name = "id_patient", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Patient patient;


    @Column(name = "id_disease", nullable = false)
    private Long idDis;

    @JoinColumn(name = "id_disease", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Disease disease;

    public Long getIdPat() {
        return idPat;
    }

    public void setIdPat(Long idPat) {
        this.idPat = idPat;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Long getIdDis() {
        return idDis;
    }

    public void setIdDis(Long idDis) {
        this.idDis = idDis;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }
}
