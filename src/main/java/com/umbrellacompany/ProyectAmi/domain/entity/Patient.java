package com.umbrellacompany.ProyectAmi.domain.entity;

import javax.persistence.*;

@Entity
@TableGenerator(name = "patient", schema = "ami")

// ESTE COMENTARIO SE HACE CON FINES DE PRUEBA PARA GIT, CON EL FIN DE VISUALIZAR CAMBIOS A NIVEL COLABORATIVO. (Jhonn luna)
// Este es el comentrio de Elkin Imbachí, cambios
//El comentario del feature-002
//Comentario hecho por Brayan Benavides

public class Patient {

    // LAS ENTIDADES PERTENECEN A LA CAPA DE ACCESO A DATOS (TABLAS) ORM -> OBJECT RELATION MAPPING

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "age", nullable = false, length = 11)
    private int age;

    @Column(name = "gender", nullable = false, length = 100)
    private String gender;

    @Column(name = "height", nullable = false)
    private float height;

    @Column(name = "weight", nullable = false)
    private float weight;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
