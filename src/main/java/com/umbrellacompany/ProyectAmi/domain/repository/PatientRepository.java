package com.umbrellacompany.ProyectAmi.domain.repository;

import com.umbrellacompany.ProyectAmi.domain.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    // CAPA DE ACCESO A DATOS - REPOSITORIO DE CONSULTAS - DAO

    // CRUD
    @Override
    List<Patient> findAll();



    Optional<Patient> findById(Long id);

    Patient save(Patient category);


    void deleteById(Long id);
}
