package com.umbrellacompany.ProyectAmi.domain.repository;

import com.umbrellacompany.ProyectAmi.domain.entity.PatientDisease;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientDiseaseRepository extends JpaRepository<PatientDisease, Long> {
}
