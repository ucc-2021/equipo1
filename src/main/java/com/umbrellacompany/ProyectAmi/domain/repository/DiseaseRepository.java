package com.umbrellacompany.ProyectAmi.domain.repository;


import com.umbrellacompany.ProyectAmi.domain.entity.Disease;
import com.umbrellacompany.ProyectAmi.domain.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiseaseRepository extends JpaRepository<Disease, Long> {

    List<Disease> findAllByOrderByName();

    @Query(value = "select ds from Disease ds order by ds.name")
    List<Disease> getAllOrderByName();

    List<Disease> findByNameContains(String name);

    //Ahora se contruye desde el Query

    @Query(value = "select ds from Disease ds where ds.name like :name")
    List<Disease> getAllByNameIsLike(@Param("name") String name);

    List<Disease> findByName(String name);
    @Query(value = "select ds from Disease ds where ds.name = :name")
        Disease getByName(@Param("name")String name);



}
