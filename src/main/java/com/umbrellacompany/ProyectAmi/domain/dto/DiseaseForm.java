package com.umbrellacompany.ProyectAmi.domain.dto;

/**
 * POJO = PLain Old Java Object - Play Load (Carga de entrada)
 */

public class DiseaseForm {

    private Long id;
    private String name;
    private String description;
    private Long id_patient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId_patient() {
        return id_patient;
    }

    public void setId_patient(Long id_patient) {
        this.id_patient = id_patient;
    }
}
