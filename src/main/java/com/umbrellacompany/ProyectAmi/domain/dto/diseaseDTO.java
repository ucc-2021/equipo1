package com.umbrellacompany.ProyectAmi.domain.dto;

public class diseaseDTO {

    private Long id;
    private String name;
    private String description;
    private patientDTO patient;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public patientDTO getPatient() {
        return patient;
    }

    public void setPatient(patientDTO patient) {
        this.patient = patient;
    }
}
