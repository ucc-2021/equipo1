package com.umbrellacompany.ProyectAmi.controller;

import com.umbrellacompany.ProyectAmi.domain.dto.DiseaseForm;
import com.umbrellacompany.ProyectAmi.domain.dto.diseaseDTO;
import com.umbrellacompany.ProyectAmi.domain.entity.Disease;
import com.umbrellacompany.ProyectAmi.service.DiseaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class DiseaseController {

    @Autowired
    private DiseaseService diseaseService;


    @GetMapping("diseases")
    public ResponseEntity<List<Disease>> getAllDiseaseOrderByName() {
        return ResponseEntity.ok(diseaseService.getAllDiseaseOrderByName());
    }

    @GetMapping("diseases/{name}")
    public ResponseEntity<List<Disease>> getAllByNameIsLike(@PathVariable String name) {
        return ResponseEntity.ok(diseaseService.getAllByNameIsLike(name));
    }

    @GetMapping("disease/{name}")
    public ResponseEntity<List<Disease>> getByName(@PathVariable String name) {
        return ResponseEntity.ok(diseaseService.getAllByName(name));
    }

    @DeleteMapping("disease")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        Disease disease = this.diseaseService.getById(id);

        if (disease.getId() != null)
            return ResponseEntity.ok("La enfermedad fue borrada con éxitp" + id);
        else
            return ResponseEntity.notFound().build();
    }

    @PostMapping("disease")
    public ResponseEntity<diseaseDTO> saveDisease(@RequestBody DiseaseForm form) {
        diseaseDTO disease = this.diseaseService.save(form);

        if (disease.getId() != null)
            return ResponseEntity.ok(disease);
        else
            return ResponseEntity.badRequest().build();
    }
}