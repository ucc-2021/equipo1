package com.umbrellacompany.ProyectAmi.controller;

import com.umbrellacompany.ProyectAmi.domain.entity.Patient;
import com.umbrellacompany.ProyectAmi.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// Esto es la API, lo expuesto al público
@RestController// se exponen endpoints

public class PatientController {

    // CAPA DE PRESENTACIÓN - CONTROLADORES TIPO REST O ENDPOINTS

    @Autowired
    private PatientService patientService;

    @GetMapping("patients")
    public ResponseEntity<List<Patient>> findAll(){
        return ResponseEntity.ok(patientService.findAll());

    }
}
