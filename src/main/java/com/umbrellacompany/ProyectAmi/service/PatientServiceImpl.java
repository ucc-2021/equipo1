package com.umbrellacompany.ProyectAmi.service;

import com.umbrellacompany.ProyectAmi.domain.entity.Patient;
import com.umbrellacompany.ProyectAmi.domain.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service // Lo expone como un servicio
@Transactional  // Si pasa algo realiza un rollback si no hace commit
public class PatientServiceImpl implements PatientService{

    @Autowired // Patrón inyección de dependencias
    private PatientRepository patientRepository;

    @Override
    public List<Patient> findAll() {
        return patientRepository.findAll();
    }

    @Override
    public Optional<Patient> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Patient save(Patient category) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
