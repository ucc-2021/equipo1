package com.umbrellacompany.ProyectAmi.service;

import com.umbrellacompany.ProyectAmi.domain.dto.DiseaseForm;
import com.umbrellacompany.ProyectAmi.domain.dto.diseaseDTO;
import com.umbrellacompany.ProyectAmi.domain.entity.Disease;
import com.umbrellacompany.ProyectAmi.domain.entity.Patient;
import com.umbrellacompany.ProyectAmi.domain.entity.PatientDisease;
import com.umbrellacompany.ProyectAmi.domain.repository.PatientDiseaseRepository;
import com.umbrellacompany.ProyectAmi.domain.repository.DiseaseRepository;
import com.umbrellacompany.ProyectAmi.domain.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional

public class DiseaseServiceImpl implements DiseaseService {

    @Autowired
    private DiseaseRepository diseaseRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private PatientDiseaseRepository diseasePatientRepository;

    @Override
    public List<Disease> getAllDiseaseOrderByName(){
        return diseaseRepository.findAllByOrderByName();

    }

    @Override
    public List<Disease> getById() {
        return null;
    }

    @Override
    public List<Disease> getAllByNameIsLike(String name) {
        return diseaseRepository.findByNameContains(name);
    }

    @Override
    public List<Disease> getAllByName(String name) {
        return diseaseRepository.findByName(name);
    }

    @Override
    public diseaseDTO save(DiseaseForm form) {

        // 1. Guardar la enfermedad
        Disease disease = new Disease();
        disease.setName(form.getName());
        disease.setDescription(form.getDescription());
        disease = diseaseRepository.save(disease);

        // 2. Validar
        Patient patient = patientRepository.findById(form.getId_patient())
                .orElseThrow(()->new IllegalArgumentException("No existe el id_patient"));

        // 3. Guardar en la tabla de ralción id patient e id disease
       // PatientDisease patientDisease = new PatientDisease();
        //PatientDisease.set

        diseaseDTO responseDTO = new diseaseDTO();
        responseDTO.setId(disease.getId());
        responseDTO.setName(disease.getName());
        responseDTO.setDescription(disease.getDescription());


        return responseDTO;
    }

    @Override
    public Disease getById(Long id) {
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        Disease disease = diseaseRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("No existe el id de la enfermedad"));
        return false;
    }
}
