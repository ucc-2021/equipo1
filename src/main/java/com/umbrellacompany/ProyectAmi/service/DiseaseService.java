package com.umbrellacompany.ProyectAmi.service;

import com.umbrellacompany.ProyectAmi.domain.dto.DiseaseForm;
import com.umbrellacompany.ProyectAmi.domain.dto.diseaseDTO;
import com.umbrellacompany.ProyectAmi.domain.entity.Disease;

import java.util.List;

public interface DiseaseService {

    List<Disease> getAllDiseaseOrderByName();

    List<Disease> getById();

    boolean deleteById(Long id);

    List<Disease> getAllByNameIsLike(String name);

    List<Disease> getAllByName(String name);

    diseaseDTO save(DiseaseForm form);


    Disease getById(Long id);
}
