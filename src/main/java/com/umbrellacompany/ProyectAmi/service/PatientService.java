package com.umbrellacompany.ProyectAmi.service;

import com.umbrellacompany.ProyectAmi.domain.entity.Patient;

import java.util.List;
import java.util.Optional;

// PATRON FACADE

public interface PatientService {

    // CAPA DE LÓGICA DE NEGOCIO - PRORAMACIÓN - TIENE SERVICIOS

    List<Patient> findAll();

    Optional<Patient> findById(Long id);

    Patient save(Patient category);


    void deleteById(Long id);
}
